import { controls } from '../../constants/controls';
import { IFighterDetails } from '../helpers/mockData';

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails) {
  return new Promise<IFighterDetails>((resolve) => {
    const {
      PlayerOneAttack,
      PlayerOneBlock,
      PlayerTwoAttack,
      PlayerTwoBlock,
      PlayerOneCriticalHitCombination,
      PlayerTwoCriticalHitCombination,
    } = controls;
    let pressedKeys = new Set<string>();
    firstFighter.position = 'left';
    secondFighter.position = 'right';

    function onKeyDown(event: KeyboardEvent) {
      if (event.repeat) return;

      pressedKeys.add(event.code);

      switch (event.code) {
        case PlayerOneAttack:
          if (!firstFighter.block && !secondFighter.block) {
            attack(firstFighter, secondFighter);
          }
          break;
        case PlayerTwoAttack:
          if (!firstFighter.block && !secondFighter.block) {
            attack(secondFighter, firstFighter);
          }
          break;
        case PlayerOneBlock:
          firstFighter.block = true;
          break;
        case PlayerTwoBlock:
          secondFighter.block = true;
          break;
      }

      if (isPlayerOneCriticalHitCombination()) {
        critAttack(firstFighter, secondFighter);
      }
      if (isPlayerTwoCriticalHitCombination()) {
        critAttack(secondFighter, firstFighter);
      }
    }

    function onKeyUp(event: KeyboardEvent): void {
      pressedKeys.delete(event.code);

      switch (event.code) {
        case PlayerOneBlock:
          firstFighter.block = false;
          break;
        case PlayerTwoBlock:
          secondFighter.block = false;
          break;
      }
    }

    function isPlayerOneCriticalHitCombination(): boolean {
      for (let key of PlayerOneCriticalHitCombination) {
        if (!pressedKeys.has(key)) return false;
      }
      return true;
    }

    function isPlayerTwoCriticalHitCombination(): boolean {
      for (let key of PlayerTwoCriticalHitCombination) {
        if (!pressedKeys.has(key)) return false;
      }
      return true;
    }

    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);
    document.addEventListener('gameOver', (event) => {
      const winner = event.loser === secondFighter ? firstFighter : secondFighter;
      resolve(winner);
    });
  });
}

export function getDamage(attacker: IFighterDetails, defender: IFighterDetails): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return Math.max(0, damage);
}

export function getHitPower(fighter: IFighterDetails): number {
  const criticalChance = Math.floor(Math.random() * 2 + 1);
  const power = fighter.attack * criticalChance;
  return power;
}

export function getCritPower(fighter: IFighterDetails): number {
  const power = fighter.attack * 2;
  return power;
}
export function getBlockPower(fighter: IFighterDetails): number {
  const dodgeChance = Math.floor(Math.random() * 2 + 1);
  const power = fighter.defense * dodgeChance;
  return power;
}

export function attack(attacker: IFighterDetails, defender: IFighterDetails): void {
  const damage = getDamage(attacker, defender);
  reduceHealth(defender, damage);
}

export function critAttack(attacker: IFighterDetails, defender: IFighterDetails): void {
  let isCooldown = attacker.isCritCooldown ?? false;

  if (isCooldown) return;

  const coolDown = 10000;
  const damage = getCritPower(attacker);
  reduceHealth(defender, damage);
  attacker.isCritCooldown = true;
  setTimeout(() => (attacker.isCritCooldown = false), coolDown);
}

export function reduceHealth(fighter: IFighterDetails, damage: number): void {
  const healthIndicator = document.querySelector(`#${fighter.position}-fighter-indicator`) as HTMLElement;
  const currentHealth = Number.parseInt(healthIndicator.style.width || '100%');
  const damageInPercent = (damage * 100) / fighter.health;
  const indicatorWidth = Math.max(0, currentHealth - damageInPercent);
  healthIndicator.style.width = `${indicatorWidth}%`;

  if (indicatorWidth === 0) {
    const event = new Event('gameOver');
    event.loser = fighter;
    document.dispatchEvent(event);
  }
}
