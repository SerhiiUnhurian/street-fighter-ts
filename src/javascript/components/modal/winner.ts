import { showModal } from './modal';
import { createFighterImage } from '../../components/fighterPreview';
import { IFighterDetails } from '../../helpers/mockData';

export function showWinnerModal(fighter: IFighterDetails): void {
  const arenaRootElement = document.querySelector('.arena___root');
  arenaRootElement!.innerHTML = '';

  showModal({
    title: `${fighter.name} won!`,
    bodyElement: createFighterImage(fighter),
    onClose: () => location.reload(),
  });
}
