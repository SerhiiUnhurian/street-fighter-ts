import { callApi } from '../helpers/apiHelper';
import { IFighter, IFighterDetails } from '../helpers/mockData';

class FighterService {
  async getFighters() {
    try {
      const endpoint: string = 'fighters.json';
      const apiResult: IFighter[] = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string) {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      const apiResult: IFighterDetails = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
